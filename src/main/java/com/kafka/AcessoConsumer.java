package com.kafka;

import com.kafka.producers.Acesso;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
public class AcessoConsumer {

    @KafkaListener(topics = "spec3-rafael-antonio-1", groupId = "seguranca")
    public void receber(@Payload Acesso acesso) {
        System.out.println("Usuário " + acesso.getIdCliente() + " com acesso " + acesso.isAcessoAtivo());
    }

}