package com.kafka.controllers;


import com.kafka.dtos.AcessoDTO;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/acesso")
public class AcessoController {


    @GetMapping("{cliente}/{porta}")
    public AcessoDTO validarAcesso(@PathVariable(value="cliente") int idcliente, @PathVariable(value="porta") int idporta)
    {

        AcessoDTO objAcessoDTO = new AcessoDTO();

        objAcessoDTO.setIdAcesso(1);
        objAcessoDTO.setAcessoAtivo(true);

        objAcessoDTO.setIdCliente(idcliente);
        objAcessoDTO.setIdPorta(idporta);

        return objAcessoDTO;

    }

}
